import React, { useState } from 'react';
import { useAuth } from '../lib/Auth';
import styles from '../styles/Home.module.css'

export const Login = () => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');

    const { logIn, signOut } = useAuth();

    const handlerLogin = (e:any) => {
        e.preventDefault();
        logIn({username, password});
        console.log(username);
    }

    return (
        <>
            <div className={styles.container}>
                <div className={styles.login_page}>
                    <div className={styles.login_form}>
                        <form className={styles.form} onSubmit={handlerLogin}>
                            <h1>Login</h1>
                            <hr />
                            <input type='text' value={username} placeholder='Username' onChange={(e) => setUsername(e.target.value)}></input>
                            <input type='password' value={password} placeholder='Password' onChange={(e) => setPassword(e.target.value)}></input>
                            <button type='submit'>Continue</button>
                            <p className={styles.message}>
                                Not registered? <a href="register">Create an account</a>
                            </p>
                        </form>
                    </div>
                </div>
            </div>

        </>
    )
}
