import { useState } from "react";
import styles from "../styles/Home.module.css";

function Register() {
  const [userName, setUsername] = useState('');
  // const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  // const RegisterMutation = gql`
  //   mutation sign($username: String!, $password: String!, $name: String!, $email: String!, $image: String!){
  //     sign(username: $username, password: $password, name: $name, email: $email, image: $image){
  //       _id
  //       username
  //       name
  //       email
  //       image
  //     }
  //   }
  // `

  return (
    <>
      <div className={styles.container}>
        <div className={styles.register_page}>
          <div className={styles.register_form}>
            <form className={styles.form}>
              <h1>Register</h1>
              <hr />
              {/* <input type='text' value={name} placeholder='Name' onChange={ (e) => setName(e.target.value)}></input> */}
              <input type='email' value={email} placeholder='Email' onChange={ (e) => setEmail(e.target.value)}></input>
              <input type='text' value={userName} placeholder='Username' onChange={ (e) => setUsername(e.target.value)}></input>
              <input type='password' value={password} placeholder='Password' onChange={ (e) => setPassword(e.target.value)}></input>
              <button>Create</button>
              <p className={styles.message}>
                Not registered? <a href="http://localhost:3000/">Sign In</a>
              </p>
            </form>
          </div>
        </div>
      </div>

    </>
  );
}
export default Register;